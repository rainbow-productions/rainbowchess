// Initialize confetti  
const confettiSettings = { target: 'confetti' };
const confetti = new ConfettiGenerator(confettiSettings);
confetti.render();

// Helper class for confetti animation
class ConfettiGenerator {
  constructor(settings) {
    this.settings = settings;
    this.confetti = [];
  }
  
  render() {
    this.confetti = [];
    for (let i = 0; i < 100; i++) {
      const confettiPiece = this.createConfettiPiece();
      this.confetti.push(confettiPiece);  
    }
    
    this.animateConfetti();
  }

  createConfettiPiece() {
    const colors = ['DodgerBlue', 'OliveDrab', 'Gold', 'Pink', 'SlateBlue', 'LightBlue', 'Violet', 'PaleGreen', 'SteelBlue', 'SandyBrown', 'Chocolate', 'Crimson'];
    const color = colors[Math.floor(Math.random() * colors.length)];

    const confetti = document.createElement('div');
    confetti.className = 'confetti';
    confetti.style.backgroundColor = color;
    confetti.style.width = '10px';
    confetti.style.height = '10px';
    confetti.style.position = 'absolute';
    confetti.style.top = '-10px';
    confetti.style.left = Math.random() * 100 + 'vw';

    return confetti;
  }

  animateConfetti() {
    this.confetti.forEach((confetti) => {
      const animationTime = Math.random() * 10 + 5; // 5-15 seconds
      const delay = Math.random() * 5; // 0-5 seconds  
      
      setTimeout(() => {
        confetti.style.top = '100vh';
      }, delay * 1000); 
      
      setTimeout(() => {
        confetti.remove(); 
      }, (delay + animationTime) * 1000);
    });
  }
}
